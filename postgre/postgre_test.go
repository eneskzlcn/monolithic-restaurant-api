package postgre

import (
	"github.com/stretchr/testify/assert"
	"restaurant-api/config"
	"testing"
)

func TestCreateNewPostgreDBWithGivenConfigRunsSuccessfully(t *testing.T) {
	config := config.DB{
		Driver:   "postgres",
		Username: "postgres",
		Password: "mypassword",
		Host:     "localhost",
		Port:     "5432",
		DBName:   "postgres",
	}
	db, err := New(config)
	if err != nil {
		assert.Error(t, err, "Error occurred when creating new db")
	}
	assert.NotEqual(t, nil, db, "")
}