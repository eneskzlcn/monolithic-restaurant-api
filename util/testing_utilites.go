package util

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func MakeRequestWithBody(method, url string, body interface{}) *http.Request{
	bodyAsByte, _ := json.Marshal(body)
	req := httptest.NewRequest(method, url, bytes.NewReader(bodyAsByte))
	req.Header.Add("Content-type", "application/json")
	return req
}
func MakeUrlWithParam(baseUrl, param string) string{
	return fmt.Sprintf("%s/%s", baseUrl, param)
}
func AssertBodyEqual(t *testing.T, responseBody io.Reader,expectedValue interface{}) {
	var actualBody interface{}
	_ = json.NewDecoder(responseBody).Decode(&actualBody)
	expectedBodyAsJSON, _ := json.Marshal(expectedValue)
	var expectedBody interface{}
	_ = json.Unmarshal(expectedBodyAsJSON, &expectedBody)
	assert.Equal(t, expectedBody, actualBody)
}
func MakeRequestWithoutBody(method, url string) *http.Request{
	req := httptest.NewRequest(method, url, http.NoBody)
	req.Header.Add("Content-type", "application/json")
	return req
}