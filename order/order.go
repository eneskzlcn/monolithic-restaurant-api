package order

import (
	"gorm.io/gorm"
	"log"
	"restaurant-api/postgre"
)

type Order struct {
	ID int `gorm:"primaryKey;column:order_id" json:"orderID"`
	CustomerID int `gorm:"column:customer_id" json:"customerID"`
	FoodID int `gorm:"column:food_id" json:"foodID"`
	CookerID int `gorm:"column:cooker_id" json:"cookerID"`
	Cooker Cooker `gorm:"foreignKey:CookerID" json:"cooker"`
}
func (Order) TableName() string {
	return postgre.CreateTableName(TABLE_PREFIX, ORDER)
}
func (o *Order) BeforeUpdate(tx *gorm.DB) error  {
	log.Printf("Order food id before update: %d", o.FoodID)
	return nil
}
func (o *Order) AfterUpdate(tx *gorm.DB) error  {
	log.Printf("Order food id after update: %d", o.FoodID)
	return nil
}

type Cooker struct {
	ID int `gorm:"primaryKey;column:cooker_id" json:"cookerID"`
	Name string `gorm:"column:cooker_name" json:"cookerName"`
}
func (Cooker) TableName() string {
	return postgre.CreateTableName(TABLE_PREFIX, COOKER)
}

const(
	COOKER = "Cooker"
	ORDER = "Order"
)
const TABLE_PREFIX = "restaurant"

