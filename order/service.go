package order

import (
	"context"
	"database/sql"
	"fmt"
	"go.uber.org/zap"
	"time"
)

type Service struct{
	orderRepository IRepository
	logger *zap.Logger
}
type IRepository interface {
	GetOrders(ctx context.Context, options sql.TxOptions) ([]Order, error)
	NewOrder(ctx context.Context, options sql.TxOptions, customerID, foodID, cookerID int) (Order, error)
	UpdateOrderFoodID(ctx context.Context,options sql.TxOptions,  orderID, foodID int) (Order, error)
	DeleteOrder(ctx context.Context,options sql.TxOptions,  orderID int) error
	GetOrderByID(ctx context.Context, options sql.TxOptions, orderID int) (Order, error)
}
func NewService(repository IRepository, logger *zap.Logger) *Service {
	return &Service{orderRepository: repository, logger:logger}
}
func (s *Service) GetOrders() ([]Order, error){
	context, cancelFunc := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancelFunc()
	orders,err := s.orderRepository.GetOrders(context, sql.TxOptions{})
	if err != nil {
		s.logger.Error(fmt.Sprintf("Orders can not taken from repository: %s",err.Error()))
		return nil, err
	}
	return orders, nil
}
func (s *Service) NewOrder(customerID, foodID, cookerID int) (Order, error){
	context, cancelFunc := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancelFunc()
	newOrder, err := s.orderRepository.NewOrder(context,sql.TxOptions{}, customerID, foodID, cookerID)
	if err != nil {
		s.logger.Error(fmt.Sprintf("Error occured when creating new order from repository: %s",err.Error()))
		return Order{}, err
	}
	return newOrder, nil
}
func (s *Service) UpdateOrderFoodID(orderID, foodID int) (Order, error){
	context, cancelFunc := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancelFunc()
	updatedOrder, err := s.orderRepository.UpdateOrderFoodID(context,sql.TxOptions{}, orderID, foodID)
	if err != nil {
		s.logger.Error(fmt.Sprintf("Error updating order food id from rep: %s",err.Error()))
		return Order{}, err
	}
	return updatedOrder, nil
}
func (s *Service) DeleteOrder(orderID int) error {
	context, cancelFunc := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancelFunc()
	err := s.orderRepository.DeleteOrder(context,sql.TxOptions{}, orderID)
	if err != nil {
		s.logger.Error(fmt.Sprintf("Error occured when deleting order from repository: %s",err.Error()))
		return err
	}
	return nil
}
func (s *Service) GetOrderByID(orderID int) (Order, error) {
	context, cancelFunc := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancelFunc()
	order, err := s.orderRepository.GetOrderByID(context, sql.TxOptions{}, orderID)
	if err != nil {
		return Order{}, err
	}
	return order, nil
}
