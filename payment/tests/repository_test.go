package tests

import (
	"github.com/stretchr/testify/assert"
	"restaurant-api/payment"
	"restaurant-api/postgre"
	"testing"
)

//- GET /payments?paymentType='CreditCard'  to get all payments (response will be the list of payments). Will be query
//params that filters result by payment type name if user wants filter.
//- POST /payments with create payment request to create a payment (response will be posted payment)
//- GET /payments/:id to get a payment by id (response will be the payment)
//- UPDATE /payments/:id with update payment request to update a specific payment (response will be the updated version of the payment)
//- DELETE /payments/:id to delete specific payment (resp will be success code)

// we need GetPayments(query ) ([]Payment, error)
func TestGetPaymentsFromDBWithGivenQueryParams(t *testing.T) {
	//need to implement models so the gorm needs them
	db ,err := postgre.New(postgre.DefaultConfig)
	assert.Nil(t, err)

	test := struct {
		 queryParams payment.GetPaymentsQuery
	}{
		queryParams: payment.GetPaymentsQuery{PaymentType: "testType"},
	}
	repo := payment.NewRepository(db)
	assert.NotNil(t, repo)

	var payments []payment.Payment
	payments, err = repo.GetPayments(test.queryParams)
	assert.Nil(t, err)
	assert.Equal(t, len(payments),1 )
}
//we need AddPayment(CreatePaymentRequest) (Payment, error)
func TestAddNewPaymentWithExistingOrderAndCustomerAndPaymentTypeParametersSuccessfully(t *testing.T) {
	//need to implement models so the gorm needs them
	db ,err := postgre.New(postgre.DefaultConfig)
	assert.Nil(t, err)

	test := struct {
		request payment.CreatePaymentRequest
	}{
		request: payment.CreatePaymentRequest{OrderID:11 , CustomerID: 3,PaymentTypeID: 1},
	}
	repo := payment.NewRepository(db)
	assert.NotNil(t, repo)
	var addedPayment payment.Payment
	addedPayment, err = repo.AddPayment(test.request)
	assert.Nil(t, err)
	assert.Equal(t, test.request.OrderID, addedPayment.OrderID )
}
//we need GetPaymentById(paymentID) (Payment, error)
func TestGetPaymentFromDbWithGivenPaymentID(t *testing.T) {
	db, err := postgre.New(postgre.DefaultConfig)
	assert.Nil(t, err)
	test := struct {
		paymentID int
	}{
		paymentID: 2,
	}
	repo := payment.NewRepository(db)
	assert.NotNil(t, repo)
	var gotPayment payment.Payment
	gotPayment, err = repo.GetPaymentByID(test.paymentID)
	assert.Nil(t, err)
	assert.NotEqual(t, payment.Payment{},gotPayment)
}
//we need UpdatePayment(UpdatePaymentRequest) (Payment, error)
func TestGetUpdatePaymentWhenTrueUpdatePaymentRequestCame(t *testing.T) {
	db, err := postgre.New(postgre.DefaultConfig)
	assert.Nil(t, err)
	test := struct {
		paymentID int
		request payment.UpdatePaymentRequest
	}{
		paymentID: 2,
		request : payment.UpdatePaymentRequest{
			OrderID:       10,
			CustomerID:    3,
			PaymentTypeID: 2,
		},
	}
	repo := payment.NewRepository(db)
	assert.NotNil(t, repo)
	var updatedPayment payment.Payment
	updatedPayment, err = repo.UpdatePayment(test.paymentID, test.request)
	assert.Nil(t, err)
	assert.Equal(t, test.request.PaymentTypeID,updatedPayment.PaymentTypeID)
}
//we need DeletePayment(paymentID) ( Payment, error)
func TestDeletePaymentByGivenPaymentID (t *testing.T) {
	db, err := postgre.New(postgre.DefaultConfig)
	assert.Nil(t, err)
	test := struct {
		paymentID int
	}{
		paymentID: 3,
	}
	repo := payment.NewRepository(db)
	assert.NotNil(t, repo)

	err = repo.DeletePayment(test.paymentID)
	assert.Nil(t, err)
}

func TestWhenCookersReturnsRequestArrivesItReturnsCookersNameAndReturOrderedByReturnnWithLimitedCount(t *testing.T) {
	//test := struct {
	//	limit int
	//
	//}{
	//	limit: 1,
	//}
	//db, err := postgre.New(postgre.DefaultConfig)
	//assert.Nil(t, err)
	//
	//repo := payment.NewRepository(db)
	//assert.NotNil(t, repo)
	//
	//var cookersReturns []payment.CookersReturn
	//cookersReturns, err = repo.GetCookersHasMostReturn(test.limit)
	assert.NotNil(t, &payment.Payment{})
	//assert.Equal(t, len(cookersReturns),test.limit)
}