package tests

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"restaurant-api/payment"
	"restaurant-api/payment/mocks"
	util "restaurant-api/util"
	"strconv"
	"testing"
)

/*
	Handler will keep the service inside and will match the related handler functions
	with services functions to provide an api. First, we need to mock the service to be able
	to test handlers as unit.
*/
const PaymentsRoute = "/payments"
func TestCreateNewHandlerWithGivenService(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockService := mocks.NewMockIService(mockCtrl)
	handlers := payment.NewHandlers(mockService)
	assert.NotNil(t, handlers)
}
func TestWhenGetPaymentsRequestArrivesWithPaymentTypeQueryParamItReturnsFilteredPayments(t *testing.T){
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	test := struct {
		query payment.GetPaymentsQuery
		expectedPayments []payment.Payment
	}{
		query : payment.GetPaymentsQuery{PaymentType: "testType"},
		expectedPayments: []payment.Payment{
			{
				ID:            2,
				CustomerID:    3,
				OrderID:       5,
				PaymentTypeID: 12,
				PaymentType:   payment.PaymentType{ID:2,Name: "testType"},
			},
		},
	}
	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().GetPayments(test.query).Return(test.expectedPayments, nil)
	handlers := payment.NewHandlers(mockService)
	assert.NotNil(t, handlers)

	app := fiber.New()
	handlers.RegisterRoutes(app)
	req := util.MakeRequestWithoutBody(fiber.MethodGet,fmt.Sprintf("%s?paymentType=%s",PaymentsRoute,test.query.PaymentType))
	resp, err := app.Test(req)

	assert.Nil(t, err)
	util.AssertBodyEqual(t,resp.Body,test.expectedPayments)
}
func TestWhenGetPaymentByIdRequestArrivesWithValidIdItReturnsThePayment(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	test := struct {
		paymentID int
		expectedPayment payment.Payment
	}{
		paymentID : 2,
		expectedPayment: payment.Payment{

			ID:            2,
			CustomerID:    3,
			OrderID:       5,
			PaymentTypeID: 12,
			PaymentType:   payment.PaymentType{ID:2,Name: "testType"},
		},
	}
	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().GetPaymentByID(test.paymentID).Return(test.expectedPayment, nil)
	handlers := payment.NewHandlers(mockService)
	assert.NotNil(t, handlers)

	app := fiber.New()
	handlers.RegisterRoutes(app)
	req := util.MakeRequestWithoutBody(fiber.MethodGet,util.MakeUrlWithParam(PaymentsRoute,strconv.Itoa(test.paymentID)))
	resp, err := app.Test(req)

	assert.Nil(t, err)
	util.AssertBodyEqual(t, resp.Body,test.expectedPayment)
}
func TestWhenUpdatePaymentRequestArrivesWithValidParamsItReturnsUpdatedPayment(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	test := struct {
		paymentID int
		expectedPayment payment.Payment
		request payment.UpdatePaymentRequest
	}{
		paymentID : 2,
		expectedPayment: payment.Payment{

			ID:            2,
			CustomerID:    3,
			OrderID:       5,
			PaymentTypeID: 12,
			PaymentType:   payment.PaymentType{ID:2,Name: "testType"},
		},
		request: payment.UpdatePaymentRequest{
			OrderID:       34,
			CustomerID:    47,
			PaymentTypeID: 52,
		},
	}
	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().UpdatePayment(test.paymentID,test.request).Return(test.expectedPayment, nil)
	handlers := payment.NewHandlers(mockService)
	assert.NotNil(t, handlers)

	app := fiber.New()
	handlers.RegisterRoutes(app)
	req := util.MakeRequestWithBody(fiber.MethodPut,util.MakeUrlWithParam(PaymentsRoute,strconv.Itoa(test.paymentID)),test.request)
	resp, err := app.Test(req)

	assert.Nil(t, err)
	util.AssertBodyEqual(t, resp.Body,test.expectedPayment)

}
func TestWhenDeletePaymentRequestArrivesWithValidPaymentIdItReturnsSucceesCode(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	test := struct {
		paymentID int
		expectedPayment payment.Payment
		request payment.UpdatePaymentRequest
	}{
		paymentID : 2,
		expectedPayment: payment.Payment{

			ID:            2,
			CustomerID:    3,
			OrderID:       5,
			PaymentTypeID: 12,
			PaymentType:   payment.PaymentType{ID:2,Name: "testType"},
		},
		request: payment.UpdatePaymentRequest{
			OrderID:       34,
			CustomerID:    47,
			PaymentTypeID: 52,
		},
	}
	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().DeletePayment(test.paymentID).Return(nil)
	handlers := payment.NewHandlers(mockService)
	assert.NotNil(t, handlers)

	app := fiber.New()
	handlers.RegisterRoutes(app)

	req := util.MakeRequestWithoutBody(fiber.MethodDelete,util.MakeUrlWithParam(PaymentsRoute,strconv.Itoa(test.paymentID)))
	resp, err := app.Test(req)
	assert.Nil(t, err)
	assert.Equal(t, fiber.StatusOK,resp.StatusCode)
}