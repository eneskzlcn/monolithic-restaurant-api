we have a payment service here to implement
- Payment Model, Customer Model Which is a foreign key to it,
- Payment Types Model which is a foreign key to payment model 
- postgre db is already implemented, we need a payment repository to execute db transactions
- need a payment service to abstract the repository from service users,
- we also have a server already, need handlers to register the service to the server

Assume that the for the BFF, we have a contract came from frontend to able to

- GET /payments?paymentType='CreditCard'  to get all payments (response will be the list of payments). Will be query
params that filters result by payment type name if user wants filter.
- POST /payments with create payment request to create a payment (response will be posted payment)
- GET /payments/:id to get a payment by id (response will be the payment)
- UPDATE /payments/:id with update payment request to update a specific payment (response will be the updated version of the payment)
- DELETE /payments/:id to delete specific payment (resp will be success code)


Todo By Steps

- Need to start from repository layer. So start to writing a test for repository to implement
TDD approach.
- After the repository layer finished, start to write the service layer test
which will be tested with a mock repository.
- After service layer finished, we need the handler layer to abstract service from
api server. So wee need to mock the service when testing the handlers.